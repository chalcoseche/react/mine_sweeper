import React, { useContext } from "react";
import { TableContext } from "./MineSweeper";
import Td from "./Td";

const Tr = ({rowIndex}) => {
  const { tableData } = useContext(TableContext);
  return (
    <tr>
      {tableData[0] && // 에러를 방지하기위해 보호연산자 넣어줌
        Array(tableData[0].length)
          .fill()
          .map((td, i) => {
            <Td rowIndex={rowIndex} cellIndex={i} />;
          })}
    </tr>
  );
};

export default Tr;
