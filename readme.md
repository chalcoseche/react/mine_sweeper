#### section 7

# 지뢰찾기 통해서 Context API 활용하기

> ## Context API 소개와 지뢰찾기

- 지뢰찾기도 2차원 배열이라 table tr td 사용

* Table Tr Td .jsx 생성

* 불필요한 렌더링을 막기위해 함수는 useCallback으로 막아준다

* onClickBtn 함수에 Context API 를 사용해 볼것
  Form 태그에다가 dispatch를 넘겨줘야함  
  `dispatch={dispatch}` 이렇게 사용하는 방법도 있지만  
  Context API 이용해서 그아래 어떠한 컴포넌트에서의 값도 **바로** 받을 수 있다.

  **부모거쳐거쳐 받아오는게아님**

> ## createContext 와 Provider

- createContext 를 불러와 준다

- 데이터들의 접근하고싶은 컴포넌트들을 ContextApi의 provider로 묶어줘야함

* 자식 컴포넌트들에서 tableData와 dispatch에 접근을 할 수 있다.

* Table context 함수 안에초기값을 설정해준다.(모양만)

* ContextAPi는 useContext와 createContext가 있다.
* 사용할 함수를 export 하고 useContext()안으로 가져온다

* 이런식으로 작성하면 안된다 / 리렌더링시에 객체가 재 생성 된다. => 자식들도 새로 리렌더링 되기 떄문  
  (contextAPI는 성능최적화가 굉장히 어려움 // )

  - 매번 새로운 객체가 생성되지 않도록 useMemo로 캐싱해준다.

* contextAPI를 useMemo로 캐싱을 한번 해줘야 성능저하가 덜 일어남.

* 작성후 자식 컴포넌트에서 dispatch를 사용 할 수 있다.

```
//지뢰 상태 코드 제작

export const CODE = {
  MINE: -7,
  NORMAL: -1,
  QUESTION: -2,
  FLAG: -3,
  QUESTION_MINE: -4,
  FLAG_MINE: -5,
  CLICKED_MINE: -6,
  OPENED: 0, // 0이상이면 전부 오픈되게 설정함
};
```

- opened 0이상이면 주변에 지뢰갯수를 알수있게 코드를 제작한것

> ## useConetext 사용해 지뢰칸 렌더링

- .jsx파일 참고

> ## 왼쪽 오른쪽 클릭 로직 작성하기

-.jsx파일 참고

- 데이터는 switch에 맞춰 처리하도록 한다.

> ## 빈칸들 한번에 열기

-.jsx파일 참고

> ## 빈칸들 한번에 열기

> ## 지뢰찾기 성능 최적화

- react dev tools 켜면 아무것도안해도 리렌더링을 함 ( 무섭 )
  (timer작동때문에 1초마다 리렌더링 함)

* memo쓸땐 하위 컴포넌트도 memo가 적용되야함(table > tr,td)

- useContext는 최적화하기어렵다, : 변할떄마다 컴포넌트가 리렌더링

* memo는 props가 안바뀌면 리렌더링 X
* useMemo는 특정값을 컴포넌트 안에서 **캐싱**
