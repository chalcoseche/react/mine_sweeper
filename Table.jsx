import React, { useContext } from "react";
import { TableContext } from "./MineSweeper";
import Tr from "./Tr";

const Table = () => {
  const { tableData } = useContext(TableContext);
  //tableContext를 넣어주면 그 value.tableData가 나오기떄문에 구조분해를 해준것
  return (
    <table>
      {/* 행/열 수로 반복문(map)돌려서 Tr.Td만들기 */}
      {Array(tableData.length)
        .fill()
        .map((tr, i) => {
          <Tr rowIndex={i} />;
        })}
    </table>
  );
};

export default Table;
