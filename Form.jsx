import React, { useState, useCallback, useContext } from "react";
import { TableContext } from "./MineSweeper";

//ContextAPi는 useContext와 createContext가 있다.

const Form = () => {
  const [row, setRow] = useState(10);
  const [cell, setCell] = useState(10);
  const [mine, setMine] = useState(20);
  /** 사용할 함수를 export 하고 useContext()안으로 가져온다 */
  // const value = useContext(TableContext);
  const { dispatch } = useContext(TableContext);

  //불필요한 렌더링을 막기위해 함수는 useCallback으로 막아준다
  const onChangeRow = useCallback(e => {
    setRow(e.target.value);
  }, []);
  const onChangeCell = useCallback(e => {
    setCell(e.target.value);
  }, []);
  const onChangeMine = useCallback(e => {
    setMine(e.target.value);
  }, []);

  // dispatch로 action을 생성후에 상위 컴포넌트에서 action을 호출한다.
  const onClickBtn = useCallback(() => {
    dispatch({ type: START_GAME, row, cell, mine });
  }, [row, cell, mine]);
  return (
    <div>
      <input
        type="number"
        placeholder="세로"
        value={row}
        onChange={onChangeRow}
      />
      <input
        type="number"
        placeholder="가로"
        value={cell}
        onChange={onChangeCell}
      />
      <input
        type="number"
        placeholder="지뢰"
        value={mine}
        onChange={onChangeMine}
      />
      <button onClick={onClickBtn}>시작</button>
    </div>
  );
};

export default Form;
